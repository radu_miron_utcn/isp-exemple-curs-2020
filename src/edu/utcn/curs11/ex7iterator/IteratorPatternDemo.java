package edu.utcn.curs11.ex7iterator;

/**
 * @author radu.miron
 */

interface Iterator {
    boolean hasNext();

    Object next();
}

interface Container {
    Iterator iterator();
}

class NameRepository implements Container {
    public String names[] = {"Robert", "John", "Julie", "Lora"};

    @Override
    public Iterator iterator() {
        return new NameIterator();
    }

    private class NameIterator implements Iterator {
        int index;

        @Override
        public boolean hasNext() {
            if (index < names.length) {
                return true;
            }
            return false;
        }

        @Override
        public Object next() {
            if (this.hasNext()) {
                return names[index++];
            }
            return null;
        }
    }
}

public class IteratorPatternDemo {
    public static void main(String[] args) {
        NameRepository namesRepository = new NameRepository();

        for (Iterator iter = namesRepository.iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            System.out.println("Name : " + name);
        }

        System.out.println("---------------\n");

        //or
        Iterator it = namesRepository.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}
