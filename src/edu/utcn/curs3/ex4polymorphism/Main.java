package edu.utcn.curs3.ex4polymorphism;

import java.util.Scanner;

/**
 * @author radumiron
 * @since 18.03.2020
 */

// TODO: Clean the code with the help of the dynamic polymorphism.
//       (The methods' overloading is considered to be o form of polymorphism (static))

public class Main {
    public static void main(String[] args) {
        System.out.println("Choose your car: 1. BMW, 2. FordGT");

        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();

        Car car;

        switch (choice) {
            case 1:
                car = new BMW320();
                break;
            case 2:
                car = new FordGT();
                break;
            default:
                throw new IllegalArgumentException("Bad choice!");
        }

        car.start();
        car.goForward();
        car.stop();

        // w/o polymorphism
//        if (choice == 1) {
//            BMW320 car = new BMW320();
//
//            car.start();
//            car.goForward();
//            car.stop();
//
//        } else {
//            FordGT car = new FordGT();
//
//            car.start();
//            car.goForward();
//            car.stop();
//        }
    }
}
