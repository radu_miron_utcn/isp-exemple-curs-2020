package edu.utcn.curs3.ex6composition;

/**
 * @author Radu Miron
 * @version 1
 */
public class Phone {
    private Battery battery;

    public Phone() {
        battery = new Battery();
    }

    public Phone(Battery battery) {
        this.battery = battery;
    }

    public void checkBatteryChargeLevel() {
        System.out.println(battery.getChargeLevel());
    }
}
