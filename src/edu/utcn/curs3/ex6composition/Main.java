package edu.utcn.curs3.ex6composition;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        // this is aggregation
        Battery battery = new Battery();
        Phone phone = new Phone(battery);
        phone.checkBatteryChargeLevel();

        // this is composition
        Phone phone1 = new Phone();
        phone1.checkBatteryChargeLevel();
    }
}
