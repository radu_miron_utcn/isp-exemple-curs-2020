package edu.utcn.curs3.ex6composition;

import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Battery {
    public Object getChargeLevel() {
        return new Random().nextInt(100);
    }
}
