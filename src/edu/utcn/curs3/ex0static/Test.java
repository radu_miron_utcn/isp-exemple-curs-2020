package edu.utcn.curs3.ex0static;

/**
 * @author Radu Miron
 * @version 1
 */
public class Test {
    private static int counter;
    private int value;

    Test(int value) {
        this.value = value;
        counter++;
    }

    public static int getCounter() {
        return counter;
    }
}
