package edu.utcn.curs3.ex0static;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Test test = new Test(1);
        Test test1 = new Test(44);

        for (int i = 0; i < 10; i++) {
            new Test(i);
        }

        Test test2 = test;

        System.out.println(Test.getCounter());
    }
}
