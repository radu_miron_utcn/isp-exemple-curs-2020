package edu.utcn.curs3.ex7staticpolymorphism;

/**
 * @author Radu Miron
 * @version 1
 */
public class BigClass {
    public void big() {
        System.out.println("big");
    }

    public void big(int n) {
        for (int i = 0; i < n; i++) {
            System.out.println("big");
        }
    }
}
