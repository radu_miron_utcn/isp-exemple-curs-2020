package edu.utcn.curs3.ex3inheritance.uisimpleexample;

import javax.swing.*;
import java.io.FileWriter;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    public Win() {
        super("My First UI in Java");
        this.setLayout(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setBounds(20, 20, 200, 200);

        JLabel label = new JLabel("Name: ");
        label.setBounds(20, 20, 50, 20);
        this.add(label);

        JTextField textField = new JTextField();
        textField.setBounds(80, 20, 90, 20);
        this.add(textField);

        JButton button = new JButton("Save");
        button.setBounds(20, 60, 170, 20);
        this.add(button);

        button.addActionListener(
                e -> {
                    try (FileWriter fileWriter = new FileWriter("/tmp/test.txt", true)) {
                        fileWriter.write(textField.getText() + "\n");
                        textField.setText("");
                        fileWriter.flush();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
        );

        this.setVisible(true);
    }
}
