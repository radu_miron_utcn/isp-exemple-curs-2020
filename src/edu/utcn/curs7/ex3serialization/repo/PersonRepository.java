package edu.utcn.curs7.ex3serialization.repo;


import edu.utcn.curs7.ex3serialization.dto.PersonDTO;

/**
 * @author radumiron
 * @since 01.04.2019
 */
public interface PersonRepository {
    void create(PersonDTO personDTO);

    PersonDTO read(String idNumber);

    void update(PersonDTO personDTO);

    void delete(String idNumber);
}
