package edu.utcn.curs7.ex0double;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        // IEEE 754 standard is used to store floating point values (i.e. double, float)
        // you can use this converter to see the actual double values //https://www.exploringbinary.com/floating-point-converter/

        double targetVal = 123.3;
        // actually this val is 123.2999999999999971578290569595992565155029296875

        double d1 = 772.3;
        // actually this value is 772.299999999999954525264911353588104248046875
        double d2 = 649;
        // this gets converted to 649

        double actualValue = d1 - d2;
        // so this is: 123.299999999999954525264911353588104248046875

        // !!! 123.299999999999954525264911353588104248046875
        //  != 123.2999999999999971578290569595992565155029296875

        System.out.println(targetVal);
        System.out.println(actualValue);

        System.out.println(targetVal == actualValue);
        // See what it prints! But why?
        // Because System.out.println(<double>) eventually calls Double.toString(<double>).
        // Look at Double.toString(<double>) JavaDoc:
        //    "* How many digits must be printed for the fractional part of
        //     * <i>m</i> or <i>a</i>? There must be at least one digit to represent
        //     * the fractional part, and beyond that as many, but only as many, more
        //     * digits as are needed to uniquely distinguish the argument value from
        //     * adjacent values of type {@code double}."
        //
        // Conclusion:
        // targetValue is represented as 123.2999999999999971578290569595992565155029296875
        // with 123.299999999999982946974341757595539093017578125
        //      and 123.2999999999999971578290569595992565155029296875
        //      as adjacent values.
        // it's enough to print '123.3' because this is closer to the initial representation than to the adjacent values.
        //
        // for the actualValue (!= targetValue), based on the same principle, 123.29999999999995 needs to be printed
    }
}
