package edu.utcn.curs8;

import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class Controller {
    private boolean collision;
    private List<Enemy> enemies;

    public Controller(List<Enemy> enemies) {
        this.enemies = enemies;
    }

    public void start() {
        while (!collision) {
            enemies.forEach(e -> {
                e.setLocation(e.getX(), e.getY() + 5);
            });

            try {
                Thread.sleep(200);
            } catch (InterruptedException ignored) {
            }
        }
    }
}
