package edu.utcn.curs8;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class GameWindow extends JFrame {

    public GameWindow(Character character, List<Enemy> enemies) {
        this.setSize(Utils.WINDOW_SIZE, Utils.WINDOW_SIZE);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLayout(null);

        character.setLocation(240, 440);

        this.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyChar() == 'a') {
                    character.setLocation(character.getX() - 5, character.getY());
                } else if (e.getKeyChar() == 'd') {
                    character.setLocation(character.getX() + 5, character.getY());
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });

        int[] c = {0};
        int distance = Utils.WINDOW_SIZE / enemies.size();
        enemies.forEach(e -> {
            e.setLocation(10 + (c[0] * distance), 10);
            GameWindow.this.add(e);
            c[0] += 1;
        });

        this.add(character);
        this.setVisible(true);
    }
}
