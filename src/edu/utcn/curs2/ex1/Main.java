package edu.utcn.curs2.ex1;

/**
 * @author Radu Miron
 */
public class Main {
    public static void main(String[] args) {
        Bicycle bicycle1 = new Bicycle("red", 20, 2, false);
        Bicycle bicycle2 = new Bicycle("blue", 21, 2, true);
        bicycle1.go();
        bicycle2.go();
        bicycle2.crash(bicycle1);
    }
}
