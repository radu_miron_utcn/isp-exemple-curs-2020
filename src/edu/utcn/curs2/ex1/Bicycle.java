package edu.utcn.curs2.ex1;

/**
 * @author Radu Miron
 */
public class Bicycle {
    private String color;
    private int wheelDiameter;
    private int numOfWheels;
    private boolean isElectrical;

    public Bicycle(String color, int wheelDiameter, int numOfWheels, boolean isElectrical) {
        this.color = color;
        this.wheelDiameter = wheelDiameter;
        this.numOfWheels = numOfWheels;
        this.isElectrical = isElectrical;
    }

    public void go() {
        System.out.println(String.format("The %s bicycle runs", color));
    }

    public void crash(Bicycle otherBicycle) {
        System.out.println(String.format("The %s bicycle crashes into %s bicycle",
                this.color, otherBicycle.color));
    }
}
