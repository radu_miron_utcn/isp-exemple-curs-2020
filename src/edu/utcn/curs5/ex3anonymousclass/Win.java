package edu.utcn.curs5.ex3anonymousclass;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {

    public Win(String name) {
        this.setSize(200, 50);
        this.setLocation(new Random().nextInt(200),
                new Random().nextInt(200));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle(name);
        JButton button = new JButton("Click!");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Win(Win.this.getTitle());
            }
        });
        this.add(button);
        this.setVisible(true);
    }
}
