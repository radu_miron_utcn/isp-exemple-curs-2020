package edu.utcn.curs5.ex2innerclass;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    private String name;

    public Win(String name) {
        this.name = name;
        this.setSize(200, 50);
        this.setLocation(new Random().nextInt(200),
                new Random().nextInt(200));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle(this.name);
        JButton button = new JButton("Click!");
        button.addActionListener(new ButtonHandler());
        this.add(button);
        this.setVisible(true);
    }

    //TODO add a counter in the window's title
    class ButtonHandler implements ActionListener {
        String name; // just for fun; remember what we talked.

        @Override
        public void actionPerformed(ActionEvent e) {
            new Win(Win.this.name + "*");
        }
    }
}
