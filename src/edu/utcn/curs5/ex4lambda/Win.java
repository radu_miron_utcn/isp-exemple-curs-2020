package edu.utcn.curs5.ex4lambda;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {

    public Win(String name) {
        this.setSize(200, 50);
        this.setLocation(new Random().nextInt(200),
                new Random().nextInt(200));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setTitle(name);
        JButton button = new JButton("Click!");

        button.addActionListener(e -> new Win(e.getActionCommand()));

        this.add(button);
        this.setVisible(true);
    }
}
