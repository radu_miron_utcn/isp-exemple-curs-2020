package edu.utcn.curs5.ex1interfacesimple;

import javax.swing.*;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    public Win() {
        this.setSize(200, 50);
        this.setLocation(new Random().nextInt(200),
                new Random().nextInt(200));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setName("Beautiful GUI");
        JButton button = new JButton("Click!");
        button.addActionListener(new ButtonHandler());
        this.add(button);
        this.setVisible(true);
    }
}
