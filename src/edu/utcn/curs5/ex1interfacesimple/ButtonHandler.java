package edu.utcn.curs5.ex1interfacesimple;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Radu Miron
 * @version 1
 */
public class ButtonHandler implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        new Win();
    }
}
