package edu.utcn.curs10.deadlock;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object l1 = new Object();
        Object l2 = new Object();

        new DyingThread(l1, l2).start();
        new DyingThread(l2, l1).start();
    }
}
