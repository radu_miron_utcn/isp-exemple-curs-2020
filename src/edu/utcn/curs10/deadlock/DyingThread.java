package edu.utcn.curs10.deadlock;

/**
 * @author Radu Miron
 * @version 1
 */
public class DyingThread extends Thread {
    private Object a;
    private Object b;

    public DyingThread(Object a, Object b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " starting");

        synchronized (a) {
            System.out.println(Thread.currentThread().getName() + " entered the first sync block");

            try {
                Thread.sleep(2);
            } catch (InterruptedException ignored) {
            }

            synchronized (b) {
                System.out.println(Thread.currentThread().getName() + " entered the second sync block");
            }
        }
    }
}
