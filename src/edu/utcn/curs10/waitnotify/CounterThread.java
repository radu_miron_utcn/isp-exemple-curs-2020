package edu.utcn.curs10.waitnotify;

/**
 * @author Radu Miron
 * @version 1
 */
public class CounterThread extends Thread {
    private Win win;

    public CounterThread(Win win) {
        this.win = win;
    }

    @Override
    public void run() {
        synchronized (Main.obj) {
            try {
                Main.obj.wait();
            } catch (InterruptedException e) {
            }

            long t1 = System.currentTimeMillis();

            try {
                Main.obj.wait();
            } catch (InterruptedException e) {
            }

            long t2 = System.currentTimeMillis();

            long delta = t2 - t1;
            win.update(delta);
        }
    }
}
