package edu.utcn.curs10.waitnotify;

import javax.swing.*;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    private JTextField tf;

    public Win() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(200, 100);
        setLayout(null);

        tf = new JTextField();
        tf.setBounds(10, 10, 180, 20);

        JButton startStop = new JButton("Start/Stop");
        startStop.setBounds(10, 30, 180, 20);
        startStop.addActionListener(e -> {
            synchronized (Main.obj) {
                Main.obj.notify();
            }
        });

        add(tf);
        add(startStop);

        setVisible(true);
    }

    public void update(long delta) {
        tf.setText(delta + "");
    }
}
