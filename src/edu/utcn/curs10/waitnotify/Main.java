package edu.utcn.curs10.waitnotify;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static final Object obj = new Object();

    public static void main(String[] args) {
        Win win = new Win();
        new CounterThread(win).start();
    }
}
