package edu.utcn.curs6.ex1equals;

import java.io.*;
import java.nio.file.Files;

/**
 * @author Radu Miron
 * @version 1
 */
public class MainInteger {
    public static void main(String[] args) {
        Integer i1 = 127;
        Integer i2 = 127;
        System.out.println(String.format("Integer %d == %d? %b", i1, i2, i1 == i2));

        Integer i3 = 128;
        Integer i4 = 128;
        System.out.println(String.format("Integer %d == %d? %b", i3, i4, i3 == i4));
        System.out.println(String.format("Integer %d.equals(%d)? %b", i3, i4, i3.equals(i4)));

        int i5 = 128;
        int i6 = 128;
        System.out.println(String.format("int %d == %d? %b", i5, i6, i5 == i6));

        System.out.println("Integers should be compared with equals()");

        String path = "/tmp/sdfsd.txt";

        File f = new File(path);

        try {
            new  FileReader(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        File tmp = new File(f.getAbsolutePath(), "sdfsd.tmp");

        try {
            Files.move(tmp.toPath(), f.toPath());
        } catch (IOException e) {
        }

    }
}
