package edu.utcn.curs6.ex1equals;

import java.util.Scanner;

/**
 * @author Radu Miron
 * @version 1
 */
public class MainString {
    public static void main(String[] args) {
        String s1 = "abc";
        String s2 = "abc";

        System.out.println(s1 + "==" + s2 + ":");
        System.out.println(s1 == s2);

        System.out.println("Input 'abc' and press <Enter>");
        Scanner scanner = new Scanner(System.in);
        String s3 = scanner.nextLine();

        System.out.println(s1 + "==" + s3 + "(second String is not cached):");
        System.out.println(s1 == s3);

        System.out.println(s1 + ".equals(" + s3 + ") (second String is still not cached):");
        System.out.println(s1.equals(s3));

        System.out.println("Strings should be compared with equals()");
    }
}
