package edu.utcn.curs4.ex6oneseconddelayprint;

import java.util.Arrays;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {

        //non-functional example
        for (String userName : Arrays.asList("fdbsdf", "dfsdfb", "svsdfvfd")) {
            System.out.println(userName);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }

        //functional example
        Arrays.asList("fdbsdf", "dfsdfb", "svsdfvfd").stream()
                .forEach(userName -> {
                    System.out.println(userName);

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                });
    }
}
