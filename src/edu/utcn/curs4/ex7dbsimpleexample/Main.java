package edu.utcn.curs4.ex7dbsimpleexample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {

    public static void main(String[] args) throws Exception {
        Connection connection = DriverManager.getConnection(
                "jdbc:mysql://(host=localhost,port=3308,user=root,password=root22)/isp");

        // insert
        Statement statement = connection.prepareStatement("INSERT INTO test VALUES (3, 'abc');");
        statement.execute("INSERT INTO test VALUES (3, 'abc');");

        // read
        ResultSet rs = statement.executeQuery("SELECT * FROM test;");
        while (rs.next()) {
            Integer id = rs.getInt("id");
            String name = rs.getString("name");

            System.out.println(id + " | " + name);
        }
    }
}
