Go to
File > Project Structure > Libraries > +
and add the driver lib/mysql-connector-java-8.0.19.jar

If you have an older mysql version installed download the corresponding driver from:
https://mvnrepository.com/artifact/mysql/mysql-connector-java

Tutorial:
https://www.tutorialspoint.com/jdbc/index.htm