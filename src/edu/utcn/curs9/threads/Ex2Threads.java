package edu.utcn.curs9.threads;

/**
 * @author Radu Miron
 * @version 1
 * @since WS 11.6
 */
public class Ex2Threads {
    public static void main(String[] args) {
        Runnable runnable = () -> printMessages();

        new Thread(runnable).start();

        new Thread(runnable).start();

        Thread.currentThread().setName("Principal");

        printMessages();
    }

    public static void printMessages() {
        for (int i = 0; i < 10; i++) {
            System.out.println(String.format("%s: message number %d",
                    Thread.currentThread().getName(), i));
            try {
                Thread.sleep(50);
            } catch (InterruptedException ignored) {
            }
        }
    }
}
