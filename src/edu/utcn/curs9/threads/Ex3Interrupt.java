package edu.utcn.curs9.threads;

/**
 * @author Radu Miron
 * @version 1
 * @since WS 11.6
 */
public class Ex3Interrupt {
    private static final int ONE_DAY_IN_MS = 1000 * 60 * 60 * 24;

    public static void main(String[] args) {
        Thread slowThred = new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + " do something");
            try {
                Thread.sleep(ONE_DAY_IN_MS);
            } catch (InterruptedException e) {
                System.err.println(Thread.currentThread().getName() + "I've been interrupted");
            }
        });
        slowThred.setName("Slow Thread");
        slowThred.start();

        Thread interrupter = new Thread(() -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ignored) {
            }

            System.out.println(Thread.currentThread().getName() + "Interrupt the other thread");
            slowThred.interrupt();
        });
        interrupter.setName("Interrupter Thread");
        interrupter.start();
    }
}
