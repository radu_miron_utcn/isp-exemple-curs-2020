package edu.utcn.curs9.threads;

/**
 * @author Radu Miron
 * @version 1
 * @since WS 11.6
 */
public class Ex1Threads {
    public static void main(String[] args) {
        new Thread() {
            @Override
            public void run() {
                printMessages();
            }
        }.start();

        new Thread() {
            @Override
            public void run() {
                printMessages();
            }
        }.start();

        Thread.currentThread().setName("Principal");

        printMessages();
    }

    public static void printMessages() {
        for (int i = 0; i < 10; i++) {
            System.out.println(String.format("%s: message number %d",
                    Thread.currentThread().getName(), i));
            try {
                Thread.sleep(50);
            } catch (InterruptedException ignored) {
            }
        }
    }
}
