package edu.utcn.curs9.threads;

import edu.utcn.curs9.filemanagers.FileReader;

import javax.swing.*;

/**
 * @author Radu Miron
 * @version 1
 */
public class Ex4ProcessFiles extends JFrame {
    public Ex4ProcessFiles() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setSize(500, 500);

        JTextField tf = new JTextField();
        tf.setBounds(10, 10, 200, 20);

        JTextArea ta = new JTextArea();
        ta.setBounds(10, 40, 300, 300);

        JButton b = new JButton("Process file");
        b.setBounds(10, 350, 200, 20);
        b.addActionListener(e ->
                new Thread(() -> {
                    String path = tf.getText();
                    try {
                        int count = FileReader.processFile(path);
                        ta.append(String.format("%s contains %d 'a' characters\n", path, count));
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(Ex4ProcessFiles.this,
                                "An error has occurred: " + ex.getMessage());
                    }
                }).start()
        );

        add(tf);
        add(ta);
        add(b);

        setVisible(true);
    }

    public static void main(String[] args) {
        new Ex4ProcessFiles();
    }
}
